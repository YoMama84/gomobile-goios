/*
Copyright 2021 The SHUMIN Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package goios

import "fmt"

type Meta struct {
	cacheMap map[string]string
	sss      []string
}

func (m *Meta) Value(k string) string {
	return m.cacheMap[k]
}

type Map struct{}

func NewMap() *Map {
	return &Map{}
}

// Test 支持
func (s *Map) SetInt(i int) {
	fmt.Println(i)
}

// map 可以这样使用TODO
func (s *Map) GetMapWithObj() *Meta {
	return &Meta{map[string]string{
		"name":  "taoshumin",
		"age":   "zhanglu",
		"sobot": "lizhi",
	}, nil}
}

// Test 支持
func (s *Map) Set(meta *Meta) {
	fmt.Println(meta.cacheMap)
	fmt.Println(meta.sss)
}

// 不支持
func (s *Map) GetMap() map[string]string {
	return map[string]string{
		"name":  "taoshumin",
		"age":   "zhanglu",
		"sobot": "lizhi",
	}
}
