/*
Copyright 2021 The SHUMIN Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package goios

import "fmt"

/*
	接口测试
*/

type Say interface {
	SayGo() string
	Gun(i interface{})        // 不支持
	Array(i ...string)        //  不支持
	Map(mp map[string]string) //  不支持
	Many(h *HistoryData)
}

type Man struct{}

func NewMan() Say {
	return &Man{}
}

func (m *Man) SayGo() string {
	return "say go"
}

// 不支持
func (m *Man) Gun(i interface{}) {
	fmt.Println("Gun: ", i)
}

// 不支持
func (m *Man) Array(i ...string) {
	fmt.Println("Array: ", i)
}

// 不支持
func (m Man) Map(mp map[string]string) {
	fmt.Println("Map: ", mp)
}

func (m Man) Many(h *HistoryData) {
	fmt.Println("Many: ", h)
}
