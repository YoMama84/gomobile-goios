/*
Copyright 2021 The SHUMIN Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package goios

/*
	数组名称测试
*/
type Rooms struct {
	peoples  []int    // 小写
	Students []string // 大写 不支持
}

func (r *Rooms) Count() int {
	return len(r.peoples)
}

func (r *Rooms) Index(i int) int {
	return r.peoples[i]
}

func Add() *Rooms {
	i := make([]int, 0)
	i = append(i, 1)
	i = append(i, 2)
	return &Rooms{peoples: i}
}
