/*
Copyright 2021 The SHUMIN Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package goios

import (
	"context"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
)

type Client struct {
	client *client.Client
}

func NewLocalClient() (*Client, error) {
	c, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, err
	}
	return &Client{client: c}, nil
}

func NewRemoteClient(host string) (*Client, error) {
	cc, err := client.NewClientWithOpts(client.WithHost(host), client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, err
	}
	return &Client{client: cc}, nil
}

func (c *Client) Close() error {
	return c.client.Close()
}

func (c *Client) GetImages() error {
	list, err := c.client.ImageList(context.Background(), types.ImageListOptions{})
	fmt.Printf("get image error: %s\n", err)
	if err != nil {
		return err
	}
	fmt.Println(list)
	return nil
}
