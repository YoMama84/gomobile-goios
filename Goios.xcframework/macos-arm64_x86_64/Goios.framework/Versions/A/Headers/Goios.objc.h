// Objective-C API for talking to x6t.io/goios Go package.
//   gobind -lang=objc x6t.io/goios
//
// File is generated by gobind. Do not edit.

#ifndef __Goios_H__
#define __Goios_H__

@import Foundation;
#include "ref.h"
#include "Universe.objc.h"


@class GoiosClient;
@class GoiosDocker;
@class GoiosHistoryData;
@class GoiosMan;
@class GoiosMap;
@class GoiosMeta;
@class GoiosRooms;
@protocol GoiosSay;
@class GoiosSay;

@protocol GoiosSay <NSObject>
// skipped method Say.Array with unsupported parameter or return types

// skipped method Say.Gun with unsupported parameter or return types

- (void)many:(GoiosHistoryData* _Nullable)h;
// skipped method Say.Map with unsupported parameter or return types

- (NSString* _Nonnull)sayGo;
@end

@interface GoiosClient : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nonnull instancetype)init;
- (BOOL)close:(NSError* _Nullable* _Nullable)error;
- (BOOL)getImages:(NSError* _Nullable* _Nullable)error;
@end

/**
 * 	切片测试
 */
@interface GoiosDocker : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nullable instancetype)init:(NSString* _Nullable)name image:(NSString* _Nullable)image;
@property (nonatomic) NSString* _Nonnull name;
@property (nonatomic) NSString* _Nonnull image;
- (GoiosHistoryData* _Nullable)get;
// skipped method Docker.GetArrays with unsupported parameter or return types

// skipped method Docker.GetMap with unsupported parameter or return types

// skipped method Docker.SetArray with unsupported parameter or return types

// skipped method Docker.SetMap with unsupported parameter or return types

@end

/**
 *  HistoryData 解决过度方案
 */
@interface GoiosHistoryData : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nonnull instancetype)init;
- (NSString* _Nonnull)messageAt:(long)i;
- (long)numMessages;
@end

@interface GoiosMan : NSObject <goSeqRefInterface, GoiosSay> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nonnull instancetype)init;
// skipped method Man.Array with unsupported parameter or return types

// skipped method Man.Gun with unsupported parameter or return types

- (void)many:(GoiosHistoryData* _Nullable)h;
// skipped method Man.Map with unsupported parameter or return types

- (NSString* _Nonnull)sayGo;
@end

@interface GoiosMap : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nullable instancetype)init;
// skipped method Map.GetMap with unsupported parameter or return types

/**
 * map 可以这样使用TODO
 */
- (GoiosMeta* _Nullable)getMapWithObj;
/**
 * Test 支持
 */
- (void)set:(GoiosMeta* _Nullable)meta;
/**
 * Test 支持
 */
- (void)setInt:(long)i;
@end

@interface GoiosMeta : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nonnull instancetype)init;
- (NSString* _Nonnull)value:(NSString* _Nullable)k;
@end

/**
 * 	数组名称测试
 */
@interface GoiosRooms : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nonnull instancetype)init;
// skipped field Rooms.Students with unsupported type: []string

- (long)count;
- (long)index:(long)i;
@end

FOUNDATION_EXPORT GoiosRooms* _Nullable GoiosAdd(void);

/**
 * 	bytes 切片测试
 */
FOUNDATION_EXPORT NSData* _Nullable GoiosGetBytes(void);

FOUNDATION_EXPORT NSString* _Nonnull GoiosGetError(NSError* _Nullable* _Nullable error);

FOUNDATION_EXPORT GoiosDocker* _Nullable GoiosNewDocker(NSString* _Nullable name, NSString* _Nullable image);

FOUNDATION_EXPORT GoiosClient* _Nullable GoiosNewLocalClient(NSError* _Nullable* _Nullable error);

FOUNDATION_EXPORT id<GoiosSay> _Nullable GoiosNewMan(void);

FOUNDATION_EXPORT GoiosMap* _Nullable GoiosNewMap(void);

FOUNDATION_EXPORT GoiosClient* _Nullable GoiosNewRemoteClient(NSString* _Nullable host, NSError* _Nullable* _Nullable error);

FOUNDATION_EXPORT NSString* _Nonnull GoiosSayHello(void);

@class GoiosSay;

@interface GoiosSay : NSObject <goSeqRefInterface, GoiosSay> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
// skipped method Say.Array with unsupported parameter or return types

// skipped method Say.Gun with unsupported parameter or return types

- (void)many:(GoiosHistoryData* _Nullable)h;
// skipped method Say.Map with unsupported parameter or return types

- (NSString* _Nonnull)sayGo;
@end

#endif
